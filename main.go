package main

import (
	"fmt"
	"github.com/gorilla/websocket"
	"time"
)

type Ping struct {
	event string
}

type Request struct {
	event   string
	channel string
}

func timeWriter(conn *websocket.Conn) {
	//ping := Ping{event: "ping"}
	//var ping []byte
	str := []byte("{\"event\":\"ping\"}")
	//request := Request{event:"subscribe"channel}
	for {
		time.Sleep(time.Second * 2)
		conn.WriteMessage(websocket.TextMessage, str)
		//conn.WriteJSON(ping)
	}
}

func main() {
	WebSocketURL := "wss://api.bitfinex.com/ws/2"

	var dialer *websocket.Dialer

	conn, _, err := dialer.Dial(WebSocketURL, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	go timeWriter(conn)
	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			fmt.Println("read:", err)
			return
		}

		fmt.Printf("received: %s\n", message)
	}
	for {
		_, message, _ := conn.ReadMessage()
		fmt.Printf("received: %s\n", message)
	}

}
